$(window).resize(function () {
    var winHeight = $(window).height();

//Preuzimanje visine footera
    var footerHeight = $('.main-footer').outerHeight();
    console.log(footerHeight);

//Davanje visine sekcijama
    $('#home,.holder, #about, #work').css('min-height', winHeight);
    console.log(winHeight);
    $('#contact').css('min-height', winHeight - footerHeight);

});
$(window).trigger('resize');

$('#custom-btn').click(function (e) {
    e.stopPropagation();
    $('nav ul').slideToggle(250);
});
$('body').click(function() {
    $('nav ul').slideUp(250);
});

//Smooth scroll
$('nav a').click(function (e) {
    e.preventDefault();
    var sectionId = $(this).attr('href');
    var sectionPosition = $(sectionId).offset().top;
    console.log('Ofset sekcije' + sectionPosition);
    //Animacija
    $('html, body').animate({
        scrollTop: sectionPosition
    }, 1000);

    //Zatvaranje navigacije na klik na link
    $('nav').removeClass('open');
});


//  Magnific popup
$(document).ready(function() {
    $('#img-holder div a').magnificPopup({
        type: 'image',
        gallery:{enabled:true},
        image: {
            titleSrc: 'title'
            // this tells the script which attribute has your caption
        }
    });
});

$('#img-holder div a').magnificPopup({
    type: 'image',
    gallery: {
        enabled: true
    }
});


// Kontakt forma
$('#contact-form').validate({
    submitHandler: function (form) {

        // Uzimanje podataka iz forme
        var data = $(form).serialize();

        // Uzimanje vrednosti iz action atributa
        var action = $(form).prop('action');

        // Onemogućavanje svih polja
        $('input, textarea, button').prop('disabled', true);
        // Promena natpisa na dugmetu
        $(form).find('button').text('Sending in progress...');

        // Slanje podataka iz forme putem AJAX metode
        $.post(
            action,
            data,
            function (response) {
                console.log(response);
                if (response == 1) {
                    // Sakrij i ukloni formu
                    $(form).slideUp(function () {
                        $(this).remove();
                    });
                    // Prikaži da je poruka uspešno poslata
                    $('.alert-success').slideDown();
                } else if ( response != '') {
                    // Ako poruka nije prosleđena - pokazaće se greška
                    alert(response);
                } else {
                    alert('Validation failed.');
                }
            }
        );
    }
});