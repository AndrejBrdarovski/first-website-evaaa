<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
          content="Looking for a neat web designer? Look no more because you are in a right place. Find me in Belgrade, Serbia, or on my site. Andrej Brdarovski over and out.">

    <title>Andrej Web Design</title>


    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/magnific-popup.css">


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-92353642-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>
<div class="container">
    <nav>
        <div class="container">
            <div id="custom-btn">
                <span></span>
                <span></span>
                <span></span>
            </div>
            <ul class="dropdown-menu">
                <li><a href="#home">Home</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#work">Work</a></li>
                <li><a href="#contact">Contact</a></li>
            </ul>
        </div>
    </nav>
</div>
<header id="home">

    <div class="holder">
        <h1><img class="img-responsive" src="img/logo.png" alt="Andrej Web Design"></h1>
        <img id="homeimg" class="img-responsive" src="img/naslovna2.png" alt="Andrej Web Design">
    </div>

</header>

<section id="about">
    <div class="holder2">
        <div class="container clearfix">
            <div class="row">
                <div class="h2-holder">
                    <h2 class="about">ABOUT</h2>
                </div>
                <div class="col-sm-9 col-sm-offset-2">
                    <p>My pesonality doesn't permit errors, likes riddles, creates solutions.
                        <br><span>Responsibility to responsivity!</span></p>
                    <p>
                        My name is <span>Andrej Brdarovski</span> and
                        one day I realised that I want to be a web
                        designer, on that same day I almost got hit by a truck,
                        <br>but that’s another <span>story</span>.
                        <br><span>Started</span> of as a carpenter and furniture designer, I graduated school of
                        <span>graphics</span>,
                        then life pushed me on a different path and I finished <span>college</span> of economics.
                        After that “truck” day, I was again set on the right track.
                        <br><span>Today</span> I am a <span>web designer</span> dedicated to <span>details</span> and
                        always
                        in search for a new <span>inspiration</span>.
                        Bicycle, Staffordshire terrier, Tai Chi and Chigong, samba, percussions, origami
                        and many more <span>beautiful</span> things are just a little part of my
                        <span>everyday</span> life, that always starts with a smile :)
                        <br>Did I mention I worked as a tattoo artist? No? Well...
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="work">
    <div class="container clearfix">
        <div class="holder3">
            <h3>WORK</h3>
        </div>
    </div>
    <div id="img-holder" class="clearfix">
        <div>
            <a href="img/mockup-asics.jpg" title="Mockup for a web shop. This was one of the homework assignments during course in Krojačeva škola web dizajna."><img class="img-responsive" src="img/asics-mockup-small.jpg" alt="Radovi"></a>
        </div>
        <div>
            <a href="img/hajmo-drugari.jpg" title="Web site for Hajmo, drugari! children's magazine (group project, site will be available soon). www.hajmodrugari.com"><img class="img-responsive" src="img/hajmo-drugari-small1.jpg" alt="Radovi"></a>
        </div>
        <div>
            <a href="img/works-big.png" title="Title"><img class="img-responsive" src="img/works.png" alt="Radovi"></a>
        </div>
        <div>
            <a href="img/works-big.png" title="Title"><img class="img-responsive" src="img/works.png" alt="Radovi"></a>
        </div>
        <div>
            <a href="img/works-big.png" title="Title"><img class="img-responsive" src="img/works.png" alt="Radovi"></a>
        </div>
        <div>
            <a href="img/works-big.png" title="Title"><img class="img-responsive" src="img/works.png" alt="Radovi"></a>
        </div>
        <div>
            <a href="img/works-big.png" title="Title"><img class="img-responsive" src="img/works.png" alt="Radovi"></a>
        </div>
        <div>
            <a href="img/works-big.png" title="Title"><img class="img-responsive" src="img/works.png" alt="Radovi"></a>
        </div>
    </div>
</section>


<section id="contact">
    <div class="container clearfix">
        <div class="h2-holder">
            <h2 class="contact">CONTACT</h2>
        </div>
        <!-- Poruka -->
        <div class="col-xs-12 col-sm-6 col-sm-offset-3">
            <div class="alert alert-success" style="display: none;">
                <strong>Your</br> message</br> has</br> been</br> sent.</strong>
            </div>
            <!-- Forma -->
            <form action="_send-email.php" method="post" id="contact-form">
                <div class="form-group">
                    <label for="SenderName">name*</label>
                    <input type="text" name="SenderName" class="form-control" id="SenderName" required>
                </div>
                <div class="form-group">
                    <label for="SenderEmail">e-mail*</label>
                    <input type="email" name="SenderEmail" class="form-control" id="SenderEmail" required>
                </div>
                <div class="form-group">
                    <label for="SenderMessage">message*</label>
                    <textarea name="SenderMessage" id="SenderMessage" class="form-control" required></textarea>
                </div>
                <button class="btn btn-primary col-xs-12">SEND</button>
            </form>
        </div>

    </div>
</section>

<footer class="main-footer" id="main-footer">
    <div class="clearfix">
        <ul>
            <li><a target="_blank" href="https://www.linkedin.com/in/andrej-brdarovski-4616aa12a/"><img
                        src="img/linkedin-32b&w.png" alt=""></a></li>
            <li><a target="_blank" href="https://www.instagram.com/andrejbrdarovski/"><img src="img/instagram-32b&w.png"
                                                                                           alt=""></a></li>
            <li><a target="_blank" href="https://www.facebook.com/AndrejBrdarovski"><img src="img/facebook-32b&w.png"
                                                                                         alt=""></a>
            </li>
        </ul>
    </div>
    <div id="small">
        <small>&copy; all rights reserved;</br> design by Andrej Brdarovski 2017</small>
    </div>
</footer>


<script src="js/jquery-3.1.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<!-- Plugins -->
<script src="js/jquery.validate.min.js"></script>
<!-- Main JS -->
<script src="js/main.js"></script>
</body>
</html>